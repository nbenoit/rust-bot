#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;
extern crate reqwest;

use rocket_contrib::json::{Json, JsonValue};
use rocket::request::LenientForm;
mod quiq;
 
/* Rust bot implementation */

fn handle_transfer(convo_id: &str, hint: &quiq::ConversationUpdateHint){
  quiq::qapi_accept_transfer(convo_id) 
}

fn handle_invitation(convo_id: &str, hint: &quiq::ConversationUpdateHint){
  quiq::qapi_accept(convo_id) 
}

fn handle_first_message(convo_id: &str, hint: &quiq::ConversationUpdateHint){
  quiq::qapi_send_message(
    convo_id,
    quiq::SendMessageRequest{text: String::from("Howdy from Rustbot!") }
  ) 
}


fn react_to_update(update: &quiq::ConversationUpdate) {
  let convo_id = update.state.id.as_ref();

  for hint in &update.hints {
    println!("Hints: {:?}", hint);
    match hint.hint.as_ref() {
      quiq::TransferRequested => handle_transfer(convo_id, hint),
      quiq::InvitationTimerActive => handle_invitation(convo_id, hint),
      quiq::NoMessageSinceAssignment => handle_first_message(convo_id, hint),
      _ => ()
    }
  }
}

#[post("/", data = "<event>")]
fn create(event: Json<quiq::Event>) -> JsonValue {
    if(event.ping){
      quiq::qapi_pong();
    }
    
    for update in &event.conversationUpdates {
      
      react_to_update(update);

      quiq::qapi_acknowledge(
        update.state.id.to_string(),
        quiq::AcknowledgeRequest{ 
          stateId: update.stateId, 
          clientState: ":D".to_string()
        }
      )
    }

    json!([]) 
}

#[get("/")]
fn read() -> JsonValue {
    json!([
        "Howdy, this is rust-bot speaking"
    ])
}


fn main() {
    rocket::ignite()
        .mount("/", routes![create])
        .mount("/", routes![read])
        .launch();
}
