
use rocket_contrib::json::{Json, JsonValue};

const site: &str = "https://<your-site>"//"https://nick.quiq.dev:41014";
const appId: &str = "<api-key>";
const appSecret: &str = "<api-secret>";

  /* Quiq Api Structs */
  
  pub const InvitationTimerActive: &str = "invitation-timer-active";
  pub const NoMessageSinceAssignment: &str = "no-message-since-assignment";
  pub const TransferRequested: &str = "transfer-requested";
  
  #[derive(Serialize, Deserialize, Debug)]
  pub struct ConversationUpdateHint{
    pub hint: String,
    pub data: JsonValue
  }
  
  #[derive(Serialize, Deserialize, Debug)]
  pub struct Conversation {
    pub id: String
  }
  
  #[derive(Serialize, Deserialize, Debug)]
  pub struct ConversationUpdate {
    pub stateId: i32,
    pub state: Conversation,
    pub hints: Vec<ConversationUpdateHint>,
    pub clientState: Option<JsonValue>
  }
  
  #[derive(Serialize, Deserialize, Debug)]
  pub struct Event {
      pub ping: bool,
      pub conversationUpdates: Vec<ConversationUpdate>
  }
  
  
  #[derive(Serialize, Deserialize, FromForm, Debug)]
  pub struct PongResponse {
    pub healthy: bool
  }
  
  
  #[derive(Serialize, Deserialize, Debug)]
  pub struct AcknowledgeRequest {
    pub stateId: i32, 
    pub clientState: String
  }
  
  
  #[derive(Serialize, Deserialize, Debug)]
  pub struct SendMessageRequest {
    pub text: String
  //  @Size(max = 10) assets: ArrayBuffer[Asset] = ArrayBuffer(),
  //  snippetId: Option[String] = None,
  //  proxyInteraction: Option[ProxyInteraction] = None,
  //  card: Option[QuiqCard] = None,
  //  quiqReply: Option[QuiqReply] = None,
  //  times: Option[QuiqTimes] = None,
  //  transcriptHints: Option[MessageTranscriptHints] = None<Paste>
  }
  
  /* Quiq Api */
  
  pub fn qapi_acknowledge(conversation_id: String, request: AcknowledgeRequest){
    let client = reqwest::Client::new(); 
    let request_url = format!(
      "{}/api/v1/messaging/conversations/{}/acknowledge", 
      site, 
      conversation_id
    );
  
    let mut response = client.post(&request_url)
      .basic_auth(
        appId,
        Some(appSecret)
      )
      .json(&request)
      .send();
  }
  
  pub fn qapi_pong() {
      let request_url = format!("{}/api/v1/agent-hooks/pong", site);
      let client = reqwest::Client::new();
      let resp = PongResponse{ healthy: true };
      let mut response = client.post(&request_url)
       .basic_auth(
         appId,
         Some(appSecret)
       )
       .json(&resp)
       .send(); 
  //     println!("{:?}", response);
  }
  
  
  pub fn qapi_accept_transfer(conversation_id: &str) {
     let request_url = format!(
        "{}/api/v1/messaging/conversations/{}/accept-transfer",
        site,
        conversation_id
        );
      let client = reqwest::Client::new();
      let mut response = client.post(&request_url)
       .basic_auth(
         appId,
         Some(appSecret)
       )
       .send();  
  }
  
  
  pub fn qapi_accept(conversation_id: &str){
      let request_url = format!(
        "{}/api/v1/messaging/conversations/{}/accept",
        site,
        conversation_id
        );
      let client = reqwest::Client::new();
      let mut response = client.post(&request_url)
       .basic_auth(
         appId,
         Some(appSecret)
       )
       .send(); 
  }
  
  pub fn qapi_send_message(conversation_id: &str, request: SendMessageRequest){
      let request_url = format!(
        "{}/api/v1/messaging/conversations/{}/send-message",
        site,
        conversation_id
        );
      let client = reqwest::Client::new();
      let mut response = client.post(&request_url)
       .basic_auth(
         appId,
         Some(appSecret)
       )
       .json(&request)
       .send(); 
       println!("Send-Message: {:?}", response);
  
  }
